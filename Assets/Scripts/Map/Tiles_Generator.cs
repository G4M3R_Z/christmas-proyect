﻿using System.Collections.Generic;
using UnityEngine;

public class Tiles_Generator : MonoBehaviour
{
    [Range(1, 5)] public int _viewCount;
    [Range(1, 100)] public int _destroyPos;

    public List<Sprite> _layers;
    public List <GameObject> _prefab;

    private void Awake()
    {
        for (int i = 0; i < _layers.Count; i++)
        {
            GameObject parent = new GameObject("Layer_0" + i);
            parent.transform.parent = transform.GetChild(0);

            parent.AddComponent<Tiles_Instance>();
            Tiles_Instance script = parent.GetComponent<Tiles_Instance>();

            StartCoroutine(script.MapTiling(_viewCount, i, _layers[i], parent.transform, _prefab[i]));
        }
    }
}