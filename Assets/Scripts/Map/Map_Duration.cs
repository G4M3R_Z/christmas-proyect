﻿using UnityEngine;

public class Map_Duration : MonoBehaviour
{
    [Range(0, 1)] public float _speed;
    Tiles_Generator _controller;
    Speed_Controller speed;

    private void Start()
    {
        _controller = GetComponentInParent<Tiles_Generator>();
        speed = GetComponentInParent<Speed_Controller>();
    }
    private void Update()
    {
        Vector2 pos = transform.position;
        pos.x -= speed._currentSpeed * Time.deltaTime * _speed;
        transform.position = pos;

        if (transform.position.x < -_controller._destroyPos) Destroy(this.gameObject);
    }
}