﻿using UnityEngine;

public class Speed_Controller : MonoBehaviour
{
    [Range(0, 30)] public float _minSpeed, _maxSpeed;
    [HideInInspector] public float _currentSpeed;

    private void Awake()
    {
        _currentSpeed = _minSpeed;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) _currentSpeed += 10;

        if (Mathf.Abs(_currentSpeed - _maxSpeed) > 0.1f)
        {
            float velocity = (_currentSpeed < _maxSpeed) ? 2 : -5;
            _currentSpeed += Time.deltaTime * velocity;
        }
        else
            _currentSpeed = _maxSpeed;
    }
}