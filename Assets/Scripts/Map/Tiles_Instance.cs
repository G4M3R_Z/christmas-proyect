﻿using System.Collections;
using UnityEngine;

public class Tiles_Instance : MonoBehaviour
{
    public IEnumerator MapTiling(int count, int layer, Sprite _layer, Transform _parent, GameObject _prefab)
    {
        while (true)
        {
            Sprite image = _layer;

            Vector3 backPos;
            backPos.x = (_parent.childCount > 0) ? _parent.GetChild(_parent.childCount - 1).position.x + image.bounds.size.x : 0;
            backPos.y = backPos.z = 0;

            GameObject instance = Instantiate(_prefab, backPos, Quaternion.identity);

            instance.transform.parent = _parent;
            instance.GetComponent<SpriteRenderer>().sprite = image;
            instance.GetComponent<SpriteRenderer>().sortingOrder = -10 * layer;

            yield return new WaitUntil(() => _parent.childCount < count);
        }
    }
}