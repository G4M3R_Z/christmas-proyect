﻿using UnityEngine;
using TMPro;

public class HighScore : MonoBehaviour
{
    public GameObject hs;
    private float highScore;

    void Awake()
    {
        highScore = PlayerPrefs.GetFloat("highScore",0);
        //if (highScore ==0){
        //	hs.SetActive(false);
        //} else {
        	hs.GetComponent<TextMeshProUGUI>().text = "Your High Score: " + highScore.ToString();
        //}
    }
}