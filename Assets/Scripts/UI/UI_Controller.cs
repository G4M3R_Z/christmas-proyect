﻿using System.Collections.Generic;
using UnityEngine;

public class UI_Controller : MonoBehaviour
{
    public GameObject _fade;
    public List<GameObject> _pauseViews;
    public Dead_Condition _dead;

    private void Awake()
    {
        SetPause(0);
    }

    #region Pause

    void SetPause(int value)
    {
        for (int i = 0; i < _pauseViews.Count; i++)
            if (i == value) _pauseViews[i].SetActive(true); else _pauseViews[i].SetActive(false);
    }
    public void PuaseButton()
    {
        if (!_dead._dead)
        {
            int value = (int)Time.timeScale;
            Time.timeScale = (value == 1) ? 0 : 1;

            SetPause(value);
        }
    }

    #endregion

    public void SceneButton(string newSceneName)
    {
        Time.timeScale = 1;
        GameObject fade = Instantiate(_fade, transform);
        fade.GetComponent<Fade_Controller>()._sceneName = newSceneName;
    }
}