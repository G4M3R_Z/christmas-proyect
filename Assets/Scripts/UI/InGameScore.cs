﻿using UnityEngine;
using TMPro;

public class InGameScore : MonoBehaviour
{
    public AudioSource _sound;
    public TextMeshProUGUI _hs, _ks;
    private Speed_Controller _speed;
    private Dead_Condition _dead;

    private float score;
    private int prevHS, kids;

    void Awake()
    {
        bool active = (PlayerPrefs.GetInt("Mute") == 0) ? false : true;
        _sound.mute = active;

        prevHS = PlayerPrefs.GetInt("HighScore",0);
        kids = PlayerPrefs.GetInt("Kids", 0);
        _ks.text = kids.ToString();

        _speed = GameObject.FindGameObjectWithTag("GameController").GetComponent<Speed_Controller>();
        _dead = GameObject.FindGameObjectWithTag("Player").GetComponent<Dead_Condition>();
    }
    void Update()
    {
        if (_dead._dead) return;

        score += _speed._currentSpeed * Time.deltaTime / 5;

        int scr = (int) score;
        if(scr > prevHS) PlayerPrefs.SetInt("HighScore", scr);

        _hs.text = scr.ToString() + " km";
        _ks.text = kids.ToString();
    }
    public void KidScore(int count)
    {
        kids += count;
        PlayerPrefs.SetInt("Kids", kids);
    }
}