﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu_Controller : MonoBehaviour
{
    public GameObject _fade;

    [Range(0, 1)] public float _minValue, _maxValue, _speed;

    [Header("Mute Button")]
    public Image _icon;
    public AudioSource _audio;
    public List<Sprite> _muteIcons;

    private void Awake()
    {
        int value = PlayerPrefs.GetInt("Mute", 0);
        _audio.mute = (value == 0) ? false : true;
        _icon.sprite = _muteIcons[value];
    }
    public void Mute()
    {
        _audio.mute = !_audio.mute;
        int value = (!_audio.mute) ? 0 : 1;
        _icon.sprite = _muteIcons[value];
        PlayerPrefs.SetInt("Mute", value);
    }
    public void SceneButton(string newSceneName)
    {
        GameObject fade = Instantiate(_fade, transform);
        fade.GetComponent<Fade_Controller>()._sceneName = newSceneName;
    }
}