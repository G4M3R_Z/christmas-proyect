﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Defeat_Controller : MonoBehaviour
{
    public int _kidsLimit;
    public TextMeshProUGUI _distanceScore, _kidScore;
    public Button _continue;
    public Animator _anim;

    private Transform _player;
    private InGameScore _score;
    private Dead_Condition _dead;

    private int _kids;

    private void Awake()
    {
        _player = GameObject.FindGameObjectWithTag("Player").transform;
        _score = GetComponentInParent<InGameScore>();
        _dead = _player.GetComponent<Dead_Condition>();
        this.gameObject.SetActive(false);
    }
    private void OnEnable()
    {
        _distanceScore.text = "Distancia maxima:: " + PlayerPrefs.GetInt("HighScore") + " km.";
        _kids = PlayerPrefs.GetInt("Kids", 0);
        _kidScore.text = "Niños recogidos: " + _kids;

        _continue.interactable = (_kids >= _kidsLimit) ? true : false;
    }
    public void Continue()
    {
        if(_kids >= _kidsLimit)
        {
            _dead._dead = false;
            _score.KidScore(-_kidsLimit);
            _anim.Play("Iddle");

            _player.position = new Vector3(-6, 0, 0);
            _player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            this.gameObject.SetActive(false);
        }
    }
}