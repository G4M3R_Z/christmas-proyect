﻿using UnityEngine;

public class Load_Screen : MonoBehaviour
{
    public GameObject _fade;
    public string _newSceneName;
    [Range(0, 10)] public float _tutorialTime;

    private void Awake()
    {
        Invoke("SkipTutorial", _tutorialTime);
    }
    public void SkipTutorial()
    {
        GameObject fade = Instantiate(_fade, transform);
        fade.GetComponent<Fade_Controller>()._sceneName = _newSceneName;
        CancelInvoke();
    }
}