﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner_Controller : MonoBehaviour
{
    public Dead_Condition _dead;
    public Transform _parent;
    [Range(0, 5)] public float _limit;
    public List <GameObject> _obstacle;
    public List<GameObject> _powerUp;
    private Speed_Controller _speed;

    private void Awake()
    {
        _speed = GetComponent<Speed_Controller>();
        StartCoroutine(SpawnerCondition());
    }
    IEnumerator SpawnerCondition()
    {
        while (true)
        {
            yield return new WaitForSeconds(5f);

            while (!_dead._dead)
            {
                int random = Random.Range(0, 5);

                switch (random)
                {
                    case 1:
                    case 2:
                    case 3: CreateObj(_obstacle[Random.Range(0, _obstacle.Count)], _parent.position); break;
                    case 4: CreateObj(_powerUp[Random.Range(0, _powerUp.Count)], _parent.position); break;
                    default: break;
                }

                yield return new WaitForSeconds(2f);
            }
        }
    }
    void CreateObj(GameObject item, Vector2 position)
    {
        Vector2 objPosition = new Vector2(position.x, Random.Range(-_limit, _limit));
        GameObject obj = Instantiate(item, objPosition, Quaternion.identity);
        obj.transform.parent = _parent;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Vector3 startPos = _parent.position; 
        Vector3 endPos = _parent.position;

        startPos.y += _limit;
        endPos.y -= _limit;

        Gizmos.DrawLine(startPos, endPos);
    }
}