﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spawner : MonoBehaviour
{
    [Range(0, 5)] public float limit;
    public GameObject obstacle;
    public GameObject missile;
    public GameObject warning;
    public Speed_Controller controller;
    
    public float obstacleSpeed, missileSpeed; //Numero que divide al Speed_Controller. Mas alto significa mas lento
    public Vector2 randomObstacle, randomMissile;//limites de timer aleatorio

    public float obstacleTimer, missileTimer;

    // Update is called once per frame
    void Start()
    {
        //StartCoroutine(ObstacleSpawn());
        StartCoroutine(MissileSpawn());
    }
    IEnumerator ObstacleSpawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(obstacleTimer);
            ResetTimer(missileTimer, randomObstacle);
            float positionY = Random.Range(-limit, limit);

            Create(obstacle, obstacleSpeed,positionY);
        }
    }
    IEnumerator MissileSpawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(missileTimer - 2f);
            ResetTimer(missileTimer, randomMissile);
            float positionY = Random.Range(-limit, limit);
            warning.SetActive(true);
            warning.transform.position = new Vector3(8f, positionY, 0);
            yield return new WaitForSeconds(2f);
            Create(missile, missileSpeed, positionY);
            warning.SetActive(false);
        }
    }
    float ResetTimer(float timer, Vector2 random)
    {
        timer = Random.Range(random.x, random.y);
        missileTimer = timer;
        return timer;
    }
    void Create(GameObject item, float speedDivider, float positionY)
    {
        Vector3 objPosition = new Vector3(transform.position.x, positionY , 0);
        GameObject obj = Instantiate(item, objPosition, transform.rotation);
        obj.GetComponent<Rigidbody2D>().velocity = Vector2.left * controller._currentSpeed / speedDivider;
        
    }
}
