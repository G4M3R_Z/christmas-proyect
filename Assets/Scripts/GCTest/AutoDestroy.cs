﻿using UnityEngine;

public class AutoDestroy : MonoBehaviour
{
    public GameObject _destructable;
    private bool _canDestroy;

    private void Awake()
    {
        Destroy(this.gameObject, 5f);
    }
    private void OnBecameVisible()
    {
        _canDestroy = true;
    }
    private void OnBecameInvisible()
    {
        if (_canDestroy)
            Destroy(_destructable);
    }
}