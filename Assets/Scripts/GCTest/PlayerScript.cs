﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    Rigidbody2D rb2d;

    [Header("Stats")]
    public int lives;
    public float invulnerabilityTimer;
    public int cookies;
    public int lifeRequirement;
    public float gravity;
    //int kids;

    [Header("Effects")]
    public bool magnet;
    public float magnetTimer;
    public int bomb;
    public bool speed;
    public float speedTimer;


    public GameObject bulletPrefab;
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        TouchInput();
        if (magnet)
        {
            Magnet();
        }



        if (invulnerabilityTimer>0)
        {
            invulnerabilityTimer -= Time.deltaTime;
            GetComponent<SpriteRenderer>().color = Color.red;
        } else
        {
            GetComponent<SpriteRenderer>().color = Color.green;
        }
    }
    
    void TouchInput()
    {
        //Touch Input
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.position.x < Screen.width / 2 && touch.phase == TouchPhase.Began)
                VerticalMovement(-gravity);//Touch in left
            else if (touch.position.x >= Screen.width / 2 && touch.phase == TouchPhase.Began)
                Shoot();//Touch in right

            if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
            {
                VerticalMovement(gravity);//Release
            }
        }

        //Mouse Input
        if (Input.GetMouseButtonDown(0))
        {
            if (Input.mousePosition.x < Screen.width / 2)
                VerticalMovement(-gravity);//Touch in left
            else if (Input.mousePosition.x >= Screen.width / 2)
                Shoot();//Touch in right
        } else if (Input.GetMouseButtonUp(0))
        {
            VerticalMovement(gravity);//Release
        }

        Vector2 position = transform.position;
        position.y = Mathf.Clamp(position.y, -4.5f, 4.5f);
        //rb2d.velocity = new Vector2(0, Mathf.Clamp(rb2d.velocity.y, -4.5f, 4.5f));
        transform.position = position;
    }

    void VerticalMovement(float direction)
    {
        rb2d.velocity = new Vector2(0, rb2d.velocity.y / 1.5f);
        rb2d.gravityScale = direction;
    }

    void Shoot()
    {
        GameObject obj = Instantiate(bulletPrefab, transform.position, transform.rotation);
        obj.GetComponent<Rigidbody2D>().AddForce(500 * Vector2.right);
    }

    void Magnet()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Cookie":
                cookies++;
                Destroy(collision.gameObject);
                break;
            case "Kid":
                cookies += 3;
                if (cookies >= 100)
                {
                    cookies -= 100;
                    lives++;
                }
                Destroy(collision.gameObject);
                break;
            case "Magnet":
                magnet = true;
                
                magnetTimer = 10;
                Destroy(collision.gameObject);
                break;
            case "":
            case "Hazard":
                if (invulnerabilityTimer <= 0)
                {
                    lives -= 1;
                    invulnerabilityTimer = 1.5f;
                    if (lives==0)
                    {
                        Destroy(gameObject);
                    }
                }
                break;
        }
    }
    
}
