﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Test : MonoBehaviour
{
    public GameObject bulletTestPrefab;
    public Rigidbody2D rb;
    public float playerSpeed;
    public float minY, maxY, limitY;
    public bool fire;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        limitY = Mathf.Clamp(transform.position.y, minY, maxY);
        transform.position = new Vector2(transform.position.x, limitY);
    }
    public void VecticalMovement()
    {
        rb.velocity = new Vector2(0, playerSpeed);      
    }
    public void Fire()
    {
        if (!fire)
        {
            GameObject bullet = Instantiate(bulletTestPrefab);
            bullet.transform.position = gameObject.transform.position;
            fire = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag ("Niño"))
        {

        }
        if(collision.CompareTag ("Efecto"))
        {

        }

    }


}
