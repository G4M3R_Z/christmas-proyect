﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMisileScript : MonoBehaviour
{
    public bool onMisil;
    public GameObject misilPrefab;
    public GameObject misilAlarmPrefab;
    public Transform player;
    public Vector2 spawnPosition;
    public float xPosition;

    void Start()
    {
        StartCoroutine(Spawn());
       
    }
    void Update()
    {
        
    }
    IEnumerator Spawn()
    {
        while (onMisil)
        {
            yield return new WaitForSeconds(3f);
            GameObject tempMisil = Instantiate(misilAlarmPrefab);
            tempMisil.transform.position = new Vector2(xPosition,player.position.y);
            
            yield return new WaitForSeconds(3f);
            GameObject actMisil = Instantiate(misilPrefab);
            actMisil.transform.position = tempMisil.transform.position;
            Destroy(tempMisil);
        }
    }
}