﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenScript : MonoBehaviour
{
    public Camera MainCamara;
    public Player_Test test;

    void Start()
    {
        MainCamara = Camera.main;
    }

    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            MovementController(touch.position);
        }
        else if (Input.GetMouseButton(0))
        {
            MovementController(Input.mousePosition);
        }
        else
        {
            test.fire = false;
        }
    }

    public void MovementController(Vector3 position)
    {
        Debug.Log(position.x) ;
        if(position.x > Screen.width / 2)
        {
            Debug.Log("RightSide");
            test.Fire();
            
        }
        else
        {
            Debug.Log("LeftSide");
            test.VecticalMovement();
        }   
    }
}
