﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Bullet_Test : MonoBehaviour
{
    Rigidbody2D rb;
    public float bulletVelocity;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();      
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(bulletVelocity, 0);
    }
    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
