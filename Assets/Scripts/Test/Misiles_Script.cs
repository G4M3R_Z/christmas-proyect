﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Misiles_Script : MonoBehaviour
{
    public float misileSpeed;
    Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        rb.velocity = new Vector2 (-10f, 0f);
    }
    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }


}
