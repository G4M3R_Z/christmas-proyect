﻿using System.Collections.Generic;
using UnityEngine;

public class Skill_Controller : MonoBehaviour
{
    [Header("Iman Config.")]
    public LayerMask _magneticLayer;
    [Range(0, 20)] public float _magnetTime, _radius;
    private float _currentTime;

    [Header("Aceleracion Config.")]
    [Range(0, 50)] public float _speedAcelerator;
    private Speed_Controller _speed;

    private void Start()
    {
        _speed = GameObject.FindGameObjectWithTag("GameController").GetComponent<Speed_Controller>();
    }
    private void Update()
    {
        if (_currentTime <= 0) return;

        MagneticArea(transform.position);
    }
    void MagneticArea(Vector3 pos)
    {
        List<Collider2D> hits = new List<Collider2D>();
        hits.AddRange(Physics2D.OverlapCircleAll(pos, _radius, _magneticLayer));

        if (hits.Count > 0)
        {
            for (int i = 0; i < hits.Count; i++)
            {
                hits[i].transform.position = Vector2.Lerp(hits[i].transform.position, pos, Time.deltaTime * 5);
            }
        }

        _currentTime -= Time.deltaTime;
        if (_currentTime <= 0) hits = new List<Collider2D>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Magnet"))
        {
            _currentTime = _magnetTime;
            Destroy(collision.gameObject);
        }

        if (collision.CompareTag("Speed"))
        {
            _speed._currentSpeed += _speedAcelerator;
            Destroy(collision.gameObject);
        }
    }
    private void OnDrawGizmos()
    {
        if(_currentTime > 0)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, _radius);
        }
    }
}