﻿using UnityEngine;

public class Dead_Condition : MonoBehaviour
{
    public GameObject _deadView, _particles;
    public Animator _anim;
    [HideInInspector] public bool _dead;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Hazard"))
        {
            _deadView.SetActive(true);

            Instantiate(_particles, transform.position, Quaternion.identity);
            _anim.Play("Dead");

            Destroy(collision.gameObject);
            _dead = true;
        }
    }
}