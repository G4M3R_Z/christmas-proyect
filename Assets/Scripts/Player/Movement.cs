﻿using UnityEngine;

public class Movement : MonoBehaviour
{
    [Header("Stats")]
    [Range(0, 5)] public float _limits;
    [Range(0, 5)] public float _gravity;

    public Animator _anim;
    public GameObject bulletPrefab;

    private Camera _cam;
    private Rigidbody2D _rgb;
    private Dead_Condition _dead;
    private InGameScore _score;

    private Transform _graphic;

    void Awake()
    {
        _cam = Camera.main;
        _rgb = GetComponent<Rigidbody2D>();
        _dead = GetComponent<Dead_Condition>();
        _score = GameObject.FindGameObjectWithTag("Canvas").GetComponent<InGameScore>();
        _graphic = transform.GetChild(0);
    }
    void Update()
    {
        Vector3 rot = _graphic.transform.localEulerAngles;
        rot.z = Mathf.LerpAngle(rot.z, _rgb.velocity.y * 2, Time.deltaTime * 5);
        _graphic.transform.localEulerAngles = rot;

        if (!_dead._dead) 
        {
            #region Touch Input

            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.touches[i];
                Vector3 touchPos = _cam.ScreenToWorldPoint(touch.position);
                bool sideValue = (touchPos.x < 0) ? true : false;

                if (sideValue) _rgb.gravityScale = -_gravity;
                else if (Input.touches[i].phase == TouchPhase.Began)
                {
                    Shoot();
                    _anim.Play("Shoot");
                }

                if (sideValue && (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled))
                    _rgb.gravityScale = _gravity;
            }

            #endregion

            #region Mouse Input

            /*if (Input.GetMouseButtonDown(0))
            {
                float side = Input.mousePosition.x;
                bool sideValue = (side < Screen.width / 2) ? true : false;

                if (sideValue) -_rgb.gravityScale = _gravity; else Shoot();
            }
            else if (Input.GetMouseButtonUp(0))
            {
                _rgb.gravityScale = _gravity;
            }*/

            #endregion
            
            #region Limits

            Vector2 position = transform.position;

            if (position.y > _limits || position.y < -_limits) _rgb.velocity = Vector2.zero;

            position.y = Mathf.Clamp(position.y, -_limits, _limits);
            transform.position = position;

        #endregion
        } 
        else
            _rgb.gravityScale = _gravity;
    }
    void Shoot()
    {
        Instantiate(bulletPrefab, transform.position, transform.rotation);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Kid"))
        {
            _score.KidScore(1);
            _anim.Play("GetKid");
            Destroy(collision.gameObject);
        }
    }
}