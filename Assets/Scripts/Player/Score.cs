﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text score;
    public int cookies;
    public int requisite;
    // Update is called once per frame
    void Update()
    {
        score.text = cookies.ToString();
    }

    public void Continue()
    {
        if (cookies >= requisite)
        {
            //Enable continue option
            cookies -= requisite;
        } else
        {
            //Enable defeat
        }
    }
}
