﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    [Range(0, 100)] public float _bossTime;
    [Range(0, 10)] public float _backPos, _limits, _dropCount, _dropTime;
    public List<GameObject> _niñosRandom;
    Transform _krampus;
    private bool _enable, _hMove, _vMove;

    private void Awake()
    {
        _krampus = transform.GetChild(0);
        StartCoroutine(Appearance());
    }
    private void Update()
    {
        if (!_enable) return;

        Vector2 pos = _krampus.localPosition;
        pos.x = (_hMove) ? Mathf.Lerp(pos.x, -_backPos, Time.deltaTime) : Mathf.Lerp(pos.x, 0, Time.deltaTime);

        if (_vMove)
        {
            float newPos = -1 + Mathf.PingPong(Time.time, 2);
            pos.y = Mathf.Lerp(pos.y, newPos * _limits, Time.deltaTime);
        }

        _krampus.localPosition = pos;
    }
    public void DropChildern(Vector2 pos)
    {
        int random = Random.Range(0, _niñosRandom.Count);
        Instantiate(_niñosRandom[random], pos, Quaternion.identity);
    }
    IEnumerator Appearance()
    {
        while (true)
        {
            yield return new WaitForSeconds(_bossTime);

            for (int i = 0; i < _dropCount; i++)
            {
                _enable = _hMove = _vMove = true;
                yield return new WaitForSeconds(Random.Range(_dropTime / 2, _dropTime * 2));

                _vMove = false;
                DropChildern(_krampus.position);

                yield return new WaitForSeconds(1.5f);
            }

            _hMove = _vMove = false;

            yield return new WaitUntil(() => _krampus.localPosition.x >= -0.1f);

            _krampus.localPosition = Vector2.zero;
            _enable = false;
        }
    }
}