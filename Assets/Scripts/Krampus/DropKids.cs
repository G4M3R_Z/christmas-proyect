﻿using System.Collections;
using UnityEngine;

public class DropKids : MonoBehaviour
{
    public Animator _anim;
    [SerializeField] private int _live;
    private BossController _boss;
    private bool _dropping;

    private void Awake()
    {
        _boss = GetComponentInParent<BossController>();
        _live = 10;
    }
    IEnumerator Drop()
    {
        _dropping = true;

        for (int i = 0; i < 5; i++)
        {
            Vector2 pos = transform.position;
            pos.y = Random.Range(-3, 3);
            _boss.DropChildern(pos);

            yield return new WaitForSeconds(0.3f);
        }

        _live = 10;
        _dropping = false;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet") && !_dropping && _live <= 0)
        {
            StartCoroutine(Drop());
            _anim.Play("Hurt");
            Destroy(collision.gameObject);
        }
        else if(collision.CompareTag("Bullet") && !_dropping)
        {
            _live -= 1;
            _anim.Play("Hurt");
            Destroy(collision.gameObject);
        }
    }
}