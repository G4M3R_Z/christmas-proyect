﻿using UnityEngine;

public class Velocity : MonoBehaviour
{
    [Range(-1, 1)] public int direction;
    [Range(0, 3)] public float _velocity;
    private Rigidbody2D _rgb;
    private Speed_Controller _speed;

    private void Awake()
    {
        _rgb = GetComponent<Rigidbody2D>();
        _speed = GameObject.FindGameObjectWithTag("GameController").GetComponent<Speed_Controller>();
    }
    void Update()
    {
        _rgb.velocity = Vector2.right * direction * _speed._currentSpeed * _velocity; 
    }
}